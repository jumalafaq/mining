Mining project 
=======================================

# Instalacja startowa dla projektu symfony4 #
## Instalacja modułów ##
```bash
composer create-project symfony/skeleton my-project
cd my-project
composer require server --dev
composer require --dev profiler
composer require sec-checker
composer require annotations
composer require twig
composer require asset
composer require doctrine maker
```

#### Run app local ####

```bash
php bin/console server:run
```

## Git ##
```bash
 git init
 git add .
 git commit -m "Initial commit"
```

## Install composer ##

```bash
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php composer-setup.php
php -r "unlink('composer-setup.php');"
php composer.phar install 
OR
composer install
```

## Install vendors ##

```bash
yarn add bootstrap-sass --dev
yarn add jquery --dev
```

## Install assets ##

#### Install node modules ####

```bash

```

#### vendors ####

```bash
 ./node_modules/.bin/encore dev


 ./node_modules/.bin/encore dev --watch


 ./node_modules/.bin/encore production


 yarn run encore dev
 yarn run encore dev --watch
 yarn run encore production
```

#### Compile js/sass ####

```bash
 ./node_modules/.bin/encore dev


 ./node_modules/.bin/encore dev --watch


 ./node_modules/.bin/encore production


 yarn run encore dev
 yarn run encore dev --watch
 yarn run encore production
```

# Run server #

```bash
php bin/console server:start
```

# Upgrade #
```bash
git pull
php ./bin/console doctrine:schema:update --force
```