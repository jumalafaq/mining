var Encore = require('@symfony/webpack-encore');
var CopyWebpackPlugin = require('copy-webpack-plugin');


Encore
// the project directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // the public path used by the web server to access the previous directory
    .setPublicPath('/build')
    // allow sass/scss files to be processed
    .enableSassLoader(function (sassOptions) {
    }, {
        resolveUrlLoader: false
    })
    .addPlugin(new CopyWebpackPlugin([
        { from: 'assets/images', to: 'images' }
    ]))

    // allow legacy applications to use $/jQuery as a global variable
    .autoProvidejQuery()

    .autoProvideVariables({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery',
    })

    .enableSourceMaps(!Encore.isProduction())

    // empty the outputPath dir before each build
    .cleanupOutputBeforeBuild()

    // show OS notifications when builds finish/fail
    // .enableBuildNotifications()

    // uncomment to create hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    .addEntry('js/app', './assets/js/app.js')
    .addEntry('js/jquery.collection', './assets/js/jquery.collection.js')
    .addStyleEntry('css/app', './assets/css/app.scss')
    .autoProvideVariables({
        Popper: ['popper.js', 'default'],
        moment: ['moment']
    })
;

module.exports = Encore.getWebpackConfig();