<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class WalletCommand extends Command
{
    protected static $defaultName = 'app:wallet';

    protected function configure()
    {
        $this
            ->setDescription('Sprawdź zawartość twojego portfela na BitBay')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $argument = $input->getArgument('arg1');

        if ($input->getOption('option1')) {
        }

        $t = print_r(json_encode($output));

        
        $io->success('You have a new command! Now make it your own! Pass --help to see your options.'.$t);
    }

}
