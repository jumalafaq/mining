<?php

namespace App\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Query\ResultSetMapping;
use App\Entity\Mines;
use App\Entity\BaseUrl;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/api")
 */
class ApiController extends Controller
{
    /**
     * @Route("/url/{id}", name="url")
     *
     * @return Response
     */
    public function urlAction($id)
    {
        $url = $this->getDoctrine()
            ->getRepository(BaseUrl::class)
            ->find($id);

        if (!$url) {
            throw $this->createNotFoundException(
                'No product found for id '.$id
            );
        }

        return new Response('Pula adresowa to: '.$url->getPool(). '<br>Znajdująca się w bazie na pozycji: '.$url->getId());
    }
    /**
     * @Route("/mediana", name="mediana")
     *
     * @return Response
     */
    public function mediana()
    {
        $id = 11;
        $name = 'yiimp';
        $date = new \DateTime();
        $date->sub(new \DateInterval('PT2H'));
        $date2 = new \DateTime();
        $date2->sub(new \DateInterval('P2M'));

        $repository = $this->getDoctrine()->getRepository('App:Mines');

//        $j = $repository->createQueryBuilder('i')
//            ->select("i.name as name")
//            ->andWhere('i.created_at > :now')
//            ->groupBy('i.name')
//            ->setMaxResults(5)
//            ->setParameter('name', $name)
////            ->setParameter('now', $date)
//            ->getQuery()
//            ->getResult();
//        $p = print_r($j);
//        return new Response( 'wynik to :  '.$p);
//        $product = $repository->findOneBy(['name' => $name]);

//        foreach($product as $value) {
//                $myArray[] = $value;
//        }
//
//


        $qb = $this->createQueryBuilder('p')
            ->andWhere('p.created_at > :date')
            ->setParameter('date', $date)
            ->orderBy('p.created_at', 'ASC')
            ->getQuery();

        return $qb->execute();


       $p = print_r($product);
        return $this->render('Default/api.html.twig', [
            'mediana' =>  $qb,
//            'bled' => $myArray,
        ]);
    }


    /**
     * @Route("/status", name="status")
     *
     * @return JsonResponse
     *
     */

    public function index()
    {
        $name = 'yiimp';
        $date = new \DateTime();
        $date->sub(new \DateInterval('PT2H'));
        $date2 = new \DateTime();
        $date2->sub(new \DateInterval('P2M'));


        $json = $this->getDoctrine()
            ->getRepository('App:Mines');

        $j = $json->createQueryBuilder('i')
            ->select("i.data as data")
            ->andWhere('i.created_at > :now')
            ->groupBy('i.name')
            ->setMaxResults(21)
//            ->setParameter('name', $name)
            ->setParameter('now', $date2)
            ->getQuery();

//
//        // Zapytanie na sztywno - dziala (wyciaga z nazwa tabelei DATA //
//        $sql = "SELECT data FROM mines WHERE created_at > '2018-01-24 14:46:06'";
//
//        $em = $this->getDoctrine()->getManager();
//        $stmt = $em->getConnection()->prepare($sql);
//        $stmt->execute();
//        $result = $stmt->fetchAll();


        // Zapytanie z komorek
        $qb = $this->getDoctrine()
            ->getRepository('App:Mines');

        $q = $qb->createQueryBuilder('g')
            ->select("g.name as name, avg(g.estimate_last24h) as estimate_last24h, avg(g.estimate_current) as estimate_current")
//            ->select("g.name as name, avg(g.estimate_last24h) as estimate_last24h, count(g.estimate_last24h) as przebiegi")
//            ->where('g.name = :name')
            ->andWhere('g.created_at > :now')
            ->groupBy('g.name')

//            ->setParameter('name', $name)
            ->setParameter('now', $date)
            ->setMaxResults(21)
            ->getQuery();

        $array1 = $j->getResult();
        $array2 = $q->getResult();
//

        // creat empty array
//        $myArray = [];
        $myArray = array();
        foreach($array1 as $value) {
            foreach($value as $key)
            $myArray[] = $key;
        }
//        foreach($array2 as $value) {
//                $myArray[] = $value;
//
//        }

//        $base = array('name' => NULL, 'port' => NULL, 'coins' => NULL, 'fees' => NULL, 'hashrate' => NULL,
//            'workers' => NULL, 'estimate_current' => NULL, 'estimate_last24h' => NULL, 'actual_last24h' => NULL, 'hashrate_last24h' => NULL,);
        $result = array_replace($array2, $myArray);

//        print_r($result);
//        dump($result);
//        exit;


//        print_r($result);

//        dump($print);
        $print = json_encode($result);
        return new Response($print);
//        return new JsonResponse(
//            [
//                'bitcore' => $result,
//
//            ]);
    }
}
