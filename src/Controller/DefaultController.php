<?php
/**
 * Created by PhpStorm.
 * User: marcinleza
 * Date: 12.01.2018
 * Time: 10:44
 */

namespace App\Controller;

use App\Entity\Mines;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Csa\Bundle\GuzzleBundle;
use Doctrine\ORM\Query\ResultSetMapping;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function index()
    {
        // you can fetch the EntityManager via $this->getDoctrine()
        // or you can add an argument to your action: index(EntityManagerInterface $em)
        $em = $this->getDoctrine()->getManager();

        $url1 = 'http://pool.hashrefinery.com/api/status';
        $url2 = 'http://api.yiimp.eu/api/status';

        $client = new \GuzzleHttp\Client();
        $response=$client->get($url1);
        $json = json_decode($response->getBody());
        $json2 = json_encode($json);

        foreach($json as $key => $value) {
//            echo $value->name . ", " . $value->estimate_last24h . ", " . $value->estimate_current . "<br>";

            $mines = new Mines();
            $mines->setPool($url1);
            $mines->setName($value->name);
            $mines->setEstimateCurrent($value->estimate_current);
            $mines->setEstimateLast24h($value->estimate_last24h);
            $mines->setData(json_encode($value));
            // tell Doctrine you want to (eventually) save the Product (no queries yet)
            $em->persist($mines);

            // actually executes the queries (i.e. the INSERT query)
            $em->flush();
        }

        return $this->render('Default/index.html.twig', [
            'res' => $json,
            'res2' => $json2,
        ]);
    }

    /**
     * @Route("/{url}", name="status_link")
     *
     * @var string $url
     *
     * @return JsonResponse
     *
     */
    public function status_link($url)
    {
        if ($url == 'hashrefinery') {
            $url ='http://pool.hashrefinery.com/api/status';
        } elseif ($url == 'yiimp') {
            $url ='http://api.yiimp.eu/api/status';
        } else{
            return $this->render('base.html.twig');
            exit;
        }
        // you can fetch the EntityManager via $this->getDoctrine()
        // or you can add an argument to your action: index(EntityManagerInterface $em)
        $em = $this->getDoctrine()->getManager();

        $client = new \GuzzleHttp\Client();
        $response=$client->get($url);
        $json = json_decode($response->getBody());
        $json2 = json_encode($json);

        foreach($json as $key => $value) {
//            echo $value->name . ", " . $value->estimate_last24h . ", " . $value->estimate_current . "<br>";

            $mines = new Mines();
            $mines->setPool($url);
            $mines->setName($value->name);
            $mines->setEstimateCurrent($value->estimate_current);
            $mines->setEstimateLast24h($value->estimate_last24h);
            $mines->setData(json_encode($value));
            // tell Doctrine you want to (eventually) save the Product (no queries yet)
            $em->persist($mines);

            // actually executes the queries (i.e. the INSERT query)
            $em->flush();
        }

        return $this->render('Default/index.html.twig', [
            'res' => $json,
            'res2' => $json2,
        ]);
    }
}