<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Query\ResultSetMapping;
use App\Entity\Mines;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/test")
 */
class TestController extends Controller
{
    /**
     * @Route("/status", name="Test_status")
     *
     */

    public function index()
    {
        $a = array('green', 'red', 'yellow');
        $b = array('avocado', 'apple', 'banana');
        $c = array_combine($a, $b);

        print_r($c);
    }
}
