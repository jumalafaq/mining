<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Csa\Bundle\GuzzleBundle;


class BitBayController extends Controller
{

    /**
     * @Route("/bitbayapi/{method}", name="bitbayapi")
     */
    public function BitBay_Trading_Api($method, $params = array())
    {
// Key necessary to work BitBay private API
        // Public key
        $key = "KEY";
        // private key
        $secret = "KEY2";

// Original BitBay code for private API
        $params["method"] = $method;
        $params["moment"] = time();
        $post = http_build_query($params, "", "&");
        $sign = hash_hmac("sha512", $post, $secret);
        $headers = array(
            "API-Key: " . $key,
            "API-Hash: " . $sign,
        );
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_URL, "https://bitbay.net/API/Trading/tradingApi.php");
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $ret = curl_exec($curl);

// Sum for locked and available coins
        // Convert to json
        $json_output = json_decode($ret, true);

        $pln_sum    = ($json_output['balances']['PLN']['available'])+($json_output['balances']['PLN']['locked']);
        $usd_sum    = ($json_output['balances']['USD']['available'])+($json_output['balances']['USD']['locked']);
        $eur_sum    = ($json_output['balances']['EUR']['available'])+($json_output['balances']['EUR']['locked']);
        $bcc_sum    = ($json_output['balances']['BCC']['available'])+($json_output['balances']['BCC']['locked']);
        $lsk_sum    = ($json_output['balances']['LSK']['available'])+($json_output['balances']['LSK']['locked']);
        $btg_sum    = ($json_output['balances']['BTG']['available'])+($json_output['balances']['BTG']['locked']);
        $kzc_sum    = ($json_output['balances']['KZC']['available'])+($json_output['balances']['KZC']['locked']);
        $xrp_sum    = ($json_output['balances']['XRP']['available'])+($json_output['balances']['XRP']['locked']);
        $eth_sum    = ($json_output['balances']['ETH']['available'])+($json_output['balances']['ETH']['locked']);
        $ltc_sum    = ($json_output['balances']['LTC']['available'])+($json_output['balances']['LTC']['locked']);
        $xin_sum    = ($json_output['balances']['XIN']['available'])+($json_output['balances']['XIN']['locked']);
        $dash_sum = ($json_output['balances']['DASH']['available'])+($json_output['balances']['DASH']['locked']);
        $game_sum = ($json_output['balances']['GAME']['available'])+($json_output['balances']['GAME']['locked']);

// Chance you currency to BTC
        function currency ($name)
        {
            //  Connect to PUBLIC API BitBay for currency
            $client = new \GuzzleHttp\Client();
            $response=$client->get('https://bitbay.net/API/Public/'.$name.'/trades.json');
            $currency = json_decode($response->getBody());
            if(is_object($currency)) return 0;
            return $currency[0]->price;
        }

        ($pln_sum == 0) ? $pln_in_btc = $pln_sum : $pln_in_btc = $pln_sum  / currency('BTCPLN') ;
        ($usd_sum == 0) ? $usd_in_btc = $usd_sum : $usd_in_btc = $usd_sum  / currency('BTCUSD') ;
        ($eur_sum == 0) ? $eur_in_btc = $eur_sum : $eur_in_btc =  $eur_sum / currency('BTCEUR') ;
        ($bcc_sum == 0) ? $bcc_in_btc = $bcc_sum : $bcc_in_btc = $bcc_sum  * currency('BCCBTC') ;
        ($lsk_sum == 0) ? $lsk_in_btc = $lsk_sum : $lsk_in_btc = $lsk_sum  * currency('LSKBTC') ;
        ($btg_sum == 0) ? $btg_in_btc = $btg_sum : $btg_in_btc = $btg_sum  * currency('BTGBTC') ;
        ($kzc_sum == 0) ? $kzc_in_btc = $kzc_sum : $kzc_in_btc = $kzc_sum  * currency('KZCBTC') ;
        ($xrp_sum == 0) ? $xrp_in_btc = $xrp_sum : $xrp_in_btc = $xrp_sum  * currency('XRPBTC') ;
        ($eth_sum == 0) ? $eth_in_btc = $eth_sum : $eth_in_btc = $eth_sum  * currency('ETHBTC') ;
        ($ltc_sum == 0) ? $ltc_in_btc = $ltc_sum : $ltc_in_btc = $ltc_sum  * currency('LTCBTC') ;
        ($xin_sum == 0) ? $xin_in_btc = $xin_sum : $xin_in_btc = $xin_sum  * currency('XINBTC') ;
        ($dash_sum == 0) ? $dash_in_btc = $dash_sum :$dash_in_btc = $dash_sum  * currency('DASHBTC') ;
        ($game_sum == 0) ? $game_in_btc = $game_sum :$game_in_btc = $game_sum  * currency('GAMEBTC') ;

// Sum for you cash in BTC
        $in_btc = round(($eur_in_btc + $bcc_in_btc + $lsk_in_btc + $usd_in_btc + $dash_in_btc + $game_in_btc + $pln_in_btc +
            $btg_in_btc + $kzc_in_btc + $xrp_in_btc + $eth_in_btc + $ltc_in_btc + $xin_in_btc), 8) ;

// Create json witch correct value
        $obj = (object) [
            'in_btc' => $in_btc,
            'coins' => [
                'PLN' => $pln_sum,
                'USD' => $usd_sum,
                'EUR' => $eur_sum,
                'BCC' => $bcc_sum,
                'LSK' => $lsk_sum,
                'BTG' => $btg_sum,
                'KZC' => $kzc_sum,
                'XRP' => $xrp_sum,
                'ETH' => $eth_sum,
                'LTC' => $ltc_sum,
                'XIN' => $xin_sum,
                'DASH' => $dash_sum,
                'GAME' => $game_sum,
            ]
        ];
        // Convert to json
        $obj = json_encode($obj);

// Save result to file
        if (file_exists("../file/data.txt")) {
            $message = "Plik został nadpisany";
            file_put_contents("../File/data.txt", $obj . "\n", FILE_APPEND);
        } else {
            $message = "Plik został utworzony";
            file_put_contents("../File/data.txt",$obj."\n");
        }

// Return new Response($obj);
        return $this->render('BitBay/index.html.twig', [
            'ret' => $ret,
            'post_data' => $obj,
            'message' => $message,
        ]);
    }
}
