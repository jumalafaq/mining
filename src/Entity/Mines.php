<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MinesRepository")
 */
class Mines
{
    /**
     * @ORM\Column(type="integer", name="id", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     *
     * @ORM\Column(type="string", length=32, name="pool", nullable=true )
     */
    protected $pool;

    /**
     *
     * @ORM\Column(type="string", length=9, name="name", nullable=true)
     */
    protected $name;

    /**
     * @ORM\Column(type="float", name="estimate_current", nullable=true)
     */
    protected $estimate_current;

    /**
     * @ORM\Column(type="float", name="estimate_last24h", nullable=true)
     */
    protected $estimate_last24h;

    /**
     * @ORM\Column(type="datetime", name="created_at", nullable=false)
     */
    protected $created_at;

    /**
     *
     * @ORM\Column(type="array", name="data", nullable=false)
     */
    protected $data;

    public function __construct()
    {
        $this->created_at = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getPool()
    {
        return $this->pool;
    }

    /**
     * @param mixed $pool
     */
    public function setPool($pool): void
    {
        $this->pool = $pool;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getEstimateCurrent()
    {
        return $this->estimate_current;
    }

    /**
     * @param mixed $estimate_current
     */
    public function setEstimateCurrent($estimate_current): void
    {
        $this->estimate_current = $estimate_current;
    }

    /**
     * @return mixed
     */
    public function getEstimateLast24h()
    {
        return $this->estimate_last24h;
    }

    /**
     * @param mixed $estimate_last24h
     */
    public function setEstimateLast24h($estimate_last24h): void
    {
        $this->estimate_last24h = $estimate_last24h;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at): void
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data): void
    {
        $this->data = $data;
    }

}
