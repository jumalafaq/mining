const $ = require('jquery');

// create global $ and jQuery variables
global.$ = global.jQuery = $;


require('bootstrap/dist/js/bootstrap.bundle.min');
require('owl.carousel/dist/owl.carousel');
require('selectize/dist/js/standalone/selectize');
require('moment/moment.js');
require('moment/locale/pl.js');
require('tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4');

jQuery(document).ready(function ($) {
    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: false,
        dots: true,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        responsive:{
            0: {
                items:1
            }
        }
    });
    $('select').selectize({
        create: true,
        sortField: {
            field: 'text',
            direction: 'asc'
        },
        dropdownParent: 'body',
        placeholder: $(this).data("placeholder"),
        allowEmptyOption: true
    });
    $('.dateFromPicker').each(function () {
        $(this).datetimepicker({
            format: "YYYY-MM-DD HH:mm",
            collapse: false,
            date: $(this).val()
        })
    });
    $('.dateToPicker').each(function () {
        $(this).datetimepicker({
            format: "YYYY-MM-DD HH:mm",
            useCurrent: false,
            collapse: false,
            date: $(this).val()
        })
    });
    $('.dateFromPicker').on("change.datetimepicker", function (e) {
        $('.dateToPicker').datetimepicker('minDate', e.date);
    });
    $('.dateToPicker').on("change.datetimepicker", function (e) {
        $('.dateFromPicker').datetimepicker('maxDate', e.date);
    });

    $('.side-button a').click(function(e){
        e.preventDefault();
        $('.side-menu').toggleClass('toggled-side');
    });
    $('.scroll-top').click(function(){
        $('html, body').animate({scrollTop : 0},800);
        return false;
    });
});